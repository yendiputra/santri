<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\DashbordController;
use App\Http\Controllers\IndexController;


Route::get('/', 'IndexController@home');
Route::get('/registrasi', 'AuthController@registrasi');
Route::get('/welcome', 'AuthController@welcome');
Route::get('/table','IndexController@table');

Route::get('/data_tables','IndexController@data_tables');
Route::get('/master', function(){
    return view('layout.master');
}
);

Route::get('cast/create', 'CastController@create');
Route::post('cast', 'CastController@store');
Route::get('cast','CastController@index');
Route::get('cast/{id}','CastController@show');
Route::get('cast/{cast_id}/edit','CastController@edit');
Route::get('cast/{cast_id}/edit','CastController@edit');
Route::put('cast/{cast_id}','CastController@update');
Route::delete('cast/{cast_id}','CastController@destroy');

Route::get('game/create', 'GameController@create');
Route::post('game', 'GameController@store');
Route::get('game','GameController@index');
Route::get('game/{id}','GameController@show');
Route::get('game/{game_id}/edit','GameController@edit');
Route::get('game/{game_id}/edit','GameController@edit');
Route::put('game/{game_id}','GameController@update');
Route::delete('game/{game_id}','GameController@destroy');

//CRUD
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('kamar', 'KamarController');
Route::resource('ustadz', 'UstadzController');