@extends('layout.master')
@section('judul')
List Data Kamar
@endsection
@section('content')
<a href="/kamar/create" class="btn btn-success">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Keterangan</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($kamar as $item)
                    <tr>
                        <td></th>
                        <td>{{$item->namakamar}}</td>
                        <td>{{$item->keterangan}}</td>
                        <td>
                            
                            <form action="/kamar/{{$item->id}}" method="POST">
                                <a href="/kamar/{{$item->id}}" class="btn btn-info">Show</a>
                            <a href="/kamar/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>Data  tidak ada</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        @endsection