@extends('layout.master')
@section('judul')
Tambah Data Kamar Santri
@endsection
@section('content')

<form action="/kamar" method="post">
    @csrf
    <div class="form-group">
      <label for="exampleFormControlInput1">Nama Kamar</label>
      <input type="text" name="namakamar" class="form-control" id="exampleFormControlInput1" placeholder="isi nama kamar">
    </div>
        @error('namakamar')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label for="exampleFormControlTextarea1">keterangan</label>
      <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
    </div>
    @error('keterangan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>

@endsection