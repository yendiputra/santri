@extends('layout.master')
@section('judul')
Edit Data kamar {{ $kamar->nama }}
@endsection
@section('content')

<form action="/kamar/{{ $kamar->id }}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="exampleFormControlInput1">Nama Kamar</label>
      <input type="text" name="namakamar" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" value={{ $kamar->namakamar }}>
    </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label for="exampleFormControlTextarea1">keterangan</label>
      <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" rows="3" >{{ $kamar->keterangan }}</textarea>
    </div>
    @error('keterangan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-warning">Update</button>    
<a href="/kamar" class="fa fa-backward"> Kembali</a>
  </form>

@endsection