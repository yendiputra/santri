<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="/admin/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">Alexander Pierce</a>
      </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        
        <li class="nav-item">
          <a href="/" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
              <span class="right badge badge-danger">New</span>
            </p>
          </a>
        </li>
        <li class="nav-item">
            <!--a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Tables
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="table" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Table</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="data_tables" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Datatables</p>
                </a>
              </li>
            </ul-->
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                  Entry Master
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="ustadz" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Ustadz</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="kamar" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Kamar</p>
                  </a>
                </li>
              </ul>
              <li class="nav-item">
                <a href="pengurus" class="nav-link">
                  <i class="fa fa-building"></i>
                  <p>Pengurus</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/pengumuman" class="nav-link">
                  <i class="fa fa-tasks"></i>
                  <p>Pengumuman</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/pengamalan" class="nav-link">
                  <i class="fa fa-graduation-cap"></i>
                  <p>Pengamalan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="logout" class="nav-link">
                  <i class="fa fa-power-off"></i>
                  <p>logout</p>
                </a>
              </li>

            
            <!--li class="nav-item">
              <a href="/cast" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Cast</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/game" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Game</p>
              </a>
            </li>
          </li-->

            </a>
          </li>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>