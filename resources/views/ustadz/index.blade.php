@extends('layout.master')
@section('judul')
List Data ustadz
@endsection
@section('content')
<a href="/ustadz/create" class="btn btn-success">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">tempatlahir</th>
                <th scope="col">tanggllahir</th>
                <th scope="col">jekel</th>
                <th scope="col">alamat</th>
                <th scope="col">hp</th>

                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($ustadz as $item)
                    <tr>
                        <td></th>
                        <td>{{$item->nama}}</td>
                        <td>{{$item->tempatlahir}}</td>
                        <td>{{$item->tanggllahir}}</td>
                        <td>{{$item->jekel}}</td>
                        <td>{{$item->alamat}}</td>
                        <td>{{$item->hp}}</td>

                        <td>
                            
                            <form action="/ustadz/{{$item->id}}" method="POST">
                                <a href="/ustadz/{{$item->id}}" class="btn btn-info">Show</a>
                            <a href="/ustadz/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>Data  tidak ada</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        @endsection