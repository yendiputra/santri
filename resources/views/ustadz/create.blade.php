@extends('layout.master')
@section('judul')
Tambah Data ustadz
@endsection
@section('content')

<form action="/ustadz" method="post">
    @csrf
    <div class="form-group">
      <label for="exampleFormControlInput1">Nama</label>
      <input type="text" name="nama" class="form-control" id="exampleFormControlInput1" placeholder="name ustadz">
    </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label for="exampleFormControlInput1">Tempat lahir</label>
        <input type="text" name="tempatlahir" class="form-control" id="exampleFormControlInput1" placeholder="tempatlahir">
      </div>
      @error('tempatlahir')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label for="exampleFormControlInput1">Tanggal lahir</label>
        <input type="date" name="tanggllahir" class="form-control" id="exampleFormControlInput1" placeholder="tanggllahir">
      </div>
      @error('tanggllahir')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
      <label for="exampleFormControlTextarea1">alamat</label>
      <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="exampleFormControlInput1">Hp</label>
      <input type="text" name="hp" class="form-control" id="exampleFormControlInput1" placeholder="temhppatlahir">
    </div>
    @error('hp')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>

@endsection