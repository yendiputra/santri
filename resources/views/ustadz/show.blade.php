@extends('layout.master')
@section('judul')
Halaman Detail ustadz {{ $ustadz->nama }}
@endsection
@section('content')

<h3>Nama ustadz : {{ $ustadz->nama }}</h3>
<p>Tempat Lahir :{{ $ustadz->tempatlahir }} </p>
<p>Tanggal Lahir :{{ $ustadz->tanggallahir }} </p>
<p>Jenis Kelamin :{{ $ustadz->jekel }} </p>
<p>alamat :{{ $ustadz->alamat }} </p>
<p>No HP :{{ $ustadz->hp }} </p>
<a href="/ustadz" class="fa fa-backward"> Kembali</a>
@endsection