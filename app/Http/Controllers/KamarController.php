<?php

namespace App\Http\Controllers;

use App\Kamar;
use Illuminate\Http\Request;

class KamarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kamar = Kamar::all(); //Fungsi untuk mengambil seluruh data pada tabel kamar

        return view('kamar.index', compact('kamar')); //Redirect ke halaman kamar/index.blade.php dengan membawa data kamar tadi
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kamar.create'); //Redirect ke halaman kamar/create.blade.php
   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'namakamar' => 'required', //nama form "title" harus diisi (required)
            'keterangan' => 'required', //nama form "writer" harus diisi (required)
        ]); //Memvalidasi inputan yang kita kirim apakah sudah benar

        Kamar::create($request->all()); //Fungsi untuk menyimpan data inputan kita

        return redirect()->route('kamar.index')
            ->with('success', 'Data Kamar Behasil dibuat.'); //Redirect ke halaman kamar/index.blade.php dengan pesan success    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kamar  $kamar
     * @return \Illuminate\Http\Response
     */
    public function show(Kamar $kamar)
    {
        return view('kamar.show', compact('kamar')); //Redirect ke halaman kamar/detail.blade.php dengan membawa data kamar sesuai ID yang dipilih
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kamar  $kamar
     * @return \Illuminate\Http\Response
     */
    public function edit(Kamar $kamar)
    {
        return view('kamar.edit', compact('kamar')); //Redirect ke halaman kamar/edit.blade.php dengan membawa data book sesuai ID yang dipilih
  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kamar  $kamar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kamar $kamar)
    {
        $request->validate([
            'namakamar' => 'required', //nama form "title" harus diisi (required)
            'keterangan' => 'required', //nama form "writer" harus diisi (required)
        ]); //Memvalidasi inputan yang kita kirim apakah sudah benar

        $kamar->update($request->all()); //Fungsi untuk mengupdate data inputan kita

        return redirect()->route('kamar.index')
            ->with('success', 'Data Kamar berhasil di updated '); //Redirect ke halaman kamar/index.blade.php dengan pesan success
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kamar  $kamar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kamar $kamar)
    {
        $kamar->delete(); //Fungsi untuk menghapus data sesuai dengan ID yang dipilih

        return redirect()->route('kamar.index')
            ->with('success', 'Kamar Berhasil di delet'); //Redirect ke halaman kamar/index.blade.php dengan pesan success    
    }
}