<?php

namespace App\Http\Controllers;

use App\Ustadz;
use Illuminate\Http\Request;
use DB;

class UstadzController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ustadz=DB::table('ustadz')->get();
        return view('ustadz.index',compact('ustadz'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ustadz.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'tempatlahir' => 'required',
            'tanggllahir' => 'required',
            'jekel' => 'required',
            'alamat' => 'required',
            'hp' => 'required',
        ]);
        $ustadz = new Ustadz;
        $ustadz->nama=$request->nama;
        $ustadz->tempatlahir=$request->tempatlahir;
        $ustadz->tanggllahir=$request->tanggllahir;
        $ustadz->jekel=$request->jekel;
        $ustadz->alamat=$request->alamat;
        $ustadz->hp=$request->hp;

        $ustadz->save();
        return redirect('/ustadz');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ustadz  $ustadz
     * @return \Illuminate\Http\Response
     */
    public function show(Ustadz $id)
    {
        $ustadz = ustadz::findOrFail($id);
    return view('ustadz.show', compact('ustadz'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ustadz  $ustadz
     * @return \Illuminate\Http\Response
     */
    public function edit(Ustadz $ustadz)
    {
        $ustadz=DB::table('ustadz')->get();
    $ustadz=ustadz::findOrFail($id);
    return view('ustadz.edit', compact('ustadz'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ustadz  $ustadz
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'nama' => 'required',
            'tempatlahir' => 'required',
            'tanggllahir' => 'required',
            'jekel' => 'required',
            'alamat' => 'required',
            'hp' => 'required',
        ]);
        $ustadz = new Ustadz;
        $ustadz->nama=$request->nama;
        $ustadz->tempatlahir=$request->tempatlahir;
        $ustadz->tanggllahir=$request->tanggllahir;
        $ustadz->jekel=$request->jekel;
        $ustadz->alamat=$request->alamat;
        $ustadz->hp=$request->hp;

        $ustadz->update();

        return redirect('/ustadz');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ustadz  $ustadz
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ustadz $id)
    {
        $ustadz = ustadz::find($id);
        $ustadz->delete();
        return redirect('/ustadz');
    }
}
