<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ustadz extends Model
{
    protected $table='ustadz';
    protected $fillable = ['nama', 'tempatlahir','tanggllahir','jekel','alamat','hp'];
}
